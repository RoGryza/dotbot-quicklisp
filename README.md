# Dotbot Quicklisp Plugin

Plugin for [Dotbot](https://github.com/anishathalye/dotbot) that installs a list of quicklisp
packages using sbcl.

## Requirements

In order to use this plugin, both sbcl and quicklisp must be installed.

The plugin assumes quicklisp is installed to `~/quicklisp`, see
(the config section)[#configuration] to change this.

## Usage

Add this repo as a submodule:

```console
$ git submodule add https://gitlab.com/RoGryza/dotbot-quicklisp.git
```

Add the plugin directory to the command in your `./install`:

```bash
"${BASEDIR}/${DOTBOT_DIR}/${DOTBOT_BIN}" -d "${BASEDIR}" --plugin-dir dotbot-quicklisp -c "${CONFIG}" "${@}"
```

List the packages you want to install in your config:

```yaml
- quicklisp:
    - swank
    - stumpwm
```

## Configuration

You can specify a package as a dictionary in order to change the default parameters:

```yaml
- quicklisp:
  - package: swank
    setup: ~/path/to/setup.el # Changes the path to quicklisp setup.el
    silent: false # When false, prints the installation command stdout
    stderr: true # When true, prints the installation command stderr
```
