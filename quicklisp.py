import os.path
import subprocess

import dotbot


class QuicklispPlugin(dotbot.Plugin):
    _directive = "quicklisp"

    _defaults = {"setup": "~/quicklisp/setup.lisp", "silent": True, "stderr": False}

    def can_handle(self, directive):
        return directive == self._directive

    def handle(self, directive, data):
        try:
            if not isinstance(data, list):
                raise ValueError("Expected a list")
            for item in data:
                params = self._normalize_parameters(item)
                self._install_package(**params)
            return True
        except ValueError as e:
            self._log.error(e)
            return False

    def _normalize_parameters(self, data):
        if isinstance(data, str):
            data = {"package": data}
        result = dict(self._defaults)
        result.update(data)

        if "package" not in result:
            raise ValueError("No package provided")
        if not isinstance(result["package"], str):
            raise ValueError("package must be a string")

        if not isinstance(result["setup"], str):
            raise ValueError("setup must be a string")
        result["setup"] = os.path.expanduser(result["setup"])
        if not os.path.isfile(result["setup"]):
            raise ValueError("{} is not a file".format(result["setup"]))

        if not isinstance(result["silent"], bool):
            raise ValueError("silent must be a bool")
        if not isinstance(result["stderr"], bool):
            raise ValueError("stderr must be a bool")

        return result

    def _install_package(self, package: str, setup: str, silent: bool, stderr: bool):
        self._log.info("Installing {}...".format(package))
        raw = "sbcl --noinform --load '{}' --non-interactive --eval \"(ql:quickload '{} {})\""
        cmd = raw.format(setup, package, ":silent t" if silent else "")

        with open(os.devnull, "w") as devnull:
            result = subprocess.call(
                cmd,
                shell=True,
                stdin=devnull,
                stdout=devnull if silent else True,
                stderr=True if stderr else devnull,
                cwd=self._context.base_directory(),
            )

            if result != 0:
                raise ValueError("Failed to install package")
